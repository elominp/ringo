# What is RingO?

RingO is a minimalist LED controller based on Arduino Framework and FastLED library.

It is designed to be adaptable to many types of LED strips even if the main targeted ones are those based on WS2812B LEDS.

The controller also has a basic human interpreter to be able to change some settings, with persistence if available, without recompiling.

## Compile-Time options

RingO has many features that can be set and controlled at compilation by defining some macros:

- `ENABLE_STREAM_CONTROLLER`: Enable interpreter features on set Arduino stream when this macro is defined to a non-0 value
- `ENABLE_STORAGE`: Enable persistence features when set to non-0 value
- `SERIAL_BAUDRATE`: Set the baudrate used by the Serial interface (required if using Serial interface - default to `9600`)
- `DATA_PIN`: Set the pin on which the LED strip data input is connected. Default to `3`
- `POWER_PIN`: Set the pin on which the LED strip power can be toggled if connected to a relay or mosfet module
- `NUM_LEDS:`: Set the numbler of LEDs on the strip
- `LED_TYPE`: Set the type of LED strip
- `LED_BRIGHTNESS`: Set the default brightness of LEDs
- `FRAME_NUMBER`: Set the maximum number of frames displayable on the LED strip
- `CIRCUIT_VOLTAGE`: Set the voltage of the circuit
- `CIRCUIT_MAX_CURRENT`: Set the maximum usable current of the circuit in milliAmps
- `DELAY_BETWEEN_TRANSITIONS`: Set the delay between two frame transition in milliseconds
- `LED_TIMER_REFRESH_RATE`: Set the refresh rate of the high priority LED strip refresh
- `LED_COMPONENT_LIMIT`: Set the maximum value of a LED color component
- `INITIAL_DELAY`: Set the initial delay in milliseconds before the controller starts
- `COMMAND_STREAM`: Set the Stream interface for the interpreter, default to `Serial`
- `DEVICE_NAME`: Set the name returned by the controller when asked to describe himself
- `VERSION_MAJOR`: Set the major version of the controller
- `VERSION_MINOR`: Set the minor version of the controller
- `VERSION_PATCH`: Set the patch version of the controller
- `DEFAULT_FADE_TRANSITIONS_STATUS`: Enable fade transitions between frames by default when set to true

## Commands

RingO has the following commands available to interact with the controller:

- Acknowledgment command: `A ${COMMAND_NAME}` -> Sent to confirm execution of a command
- Non-Acknowledgment command: `N ${COMMAND_NAME}` -> Sent to alert of an error when executing the command
- Brightness command: `B [0-255]` -> Use this command to set the current brightness of the LEDs
- Frame command: `F ${INDEX} ${red green blue}*${NUMBER_OF_LEDS} ` -> Enable to change a frame
- Power command: `P ${0, 1}` -> Turn on/off the power module of the LED strip
- Current limit command: `C ${MAX}` -> Set the maximum usable current
- Identify command: `I` -> Ask the controller to describe itself
- Delay command: `D ${DURATION}` -> Set the delay in milliseconds between 2 frames
- Refresh rate command: `R ${RATE}` -> Set the frequency between frames transitions
- Transition command: `T ${0, 1}` -> Turn on/off transitionning between frames. Turning it off means the LED will continue displaying the current frame

If the device has persistent storage capabilities and the commands are enabled at compile-time it will add the following commands:

- Save command: `S` -> Save the current settings in the memory of the device
- Load command: `L` -> Load the settings from the memory of the device and replace current settings
- Erase command: `E` -> Erase the settings from the memory of the device

All commands must be terminated by a line feeding whatever the format (Windows `\\r\\n` or Linux `\\n` or MacOS `\\r`) and the controller will return an ACK (`A`) or a NACK (`N`) command.

Some examples are available in the file `RingO/extras/command_examples.txt`
