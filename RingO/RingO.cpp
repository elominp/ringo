/*
 * This file is part of the Ringo distribution (https://gitlab.com/elominp/ringo/).
 * Copyright (c) 2021 Guillaume Pirou.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__) || \
    defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__) || \
    defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega16U4__) || \
    defined(__AVR_ATmega8__)
#ifndef ENABLE_STORAGE
#define ENABLE_STORAGE 1
#endif // !ENABLE_STORAGE
#else
#ifndef ENABLE_STORAGE
#define ENABLE_STORAGE 0
#endif // !ENABLE_STORAGE
#endif

#ifndef ENABLE_STREAM_CONTROLLER
#ifndef __AVR_ATmega8__
#define ENABLE_STREAM_CONTROLLER 1
#endif // !__AVR_ATmega8__
#endif // !ENABLE_STREAM_CONTROLLER

#if ENABLE_STORAGE && !ENABLE_STREAM_CONTROLLER
#pragma message "Storage can't be enabled without stream controller, disabling it!"
#undef ENABLE_STORAGE
#define ENABLE_STORAGE 0
#endif // ENABLE_STORAGE && !ENABLE_STREAM_CONTROLLER

#include <Arduino.h>
#if ENABLE_STORAGE
// TODO: Implement store/load of pixel frames
#include <EEPROM.h>
#endif // ENABLE_STORAGE
#include <FastLED.h>
#ifdef __SAM3X8E__
#include <DueTimer.h>
#endif // __SAM3X8E__
#include <string.h>
#ifdef __AVR__
#include <avr/interrupt.h>
#include <avr/sleep.h>
#endif // __AVR__

#if FASTLED_VERSION < 3004000
#error "FASTLED version too old, this program requires FASTLED >= 3.004.000"
#endif // FASTLED_VERSION < 3004000

#ifndef SERIAL_BAUDRATE
#define SERIAL_BAUDRATE 9600
#endif // !SERIAL_BAUDRATE

#ifndef DATA_PIN
#define DATA_PIN 3
#endif // !DATA_PIN

#ifndef POWER_PIN
#define POWER_PIN 7
#endif // !POWER_PIN

#ifndef NUM_LEDS
#define NUM_LEDS 12
#endif // !NUM_LEDS

#ifndef LED_TYPE
#define LED_TYPE NEOPIXEL
#endif // !LED_TYPE

#ifndef LED_BRIGHTNESS
#define LED_BRIGHTNESS 255
#endif // !LED_BRIGHTNESS

#ifndef FRAME_NUMBER
#define FRAME_NUMBER 7
#endif // !FRAME_NUMBER

#ifndef CIRCUIT_VOLTAGE
#define CIRCUIT_VOLTAGE 5
#endif // !CIRCUIT_VOLTAGE

#ifndef CIRCUIT_MAX_CURRENT
#define CIRCUIT_MAX_CURRENT 500
#endif // !CIRCUIT_MAX_CURRENT

#ifndef DELAY_BETWEEN_TRANSITIONS
#define DELAY_BETWEEN_TRANSITIONS 10000
#endif // !DELAY_BETWEEN_TRANSITIONS

#ifndef LED_TIMER_REFRESH_RATE
#define LED_TIMER_REFRESH_RATE 60
#endif // !LED_TIMER_REFRESH_RATE

#ifndef LED_COMPONENT_LIMIT
#define LED_COMPONENT_LIMIT 255
#endif // !LED_COMPONENT_LIMIT

#ifndef INITIAL_DELAY
#define INITIAL_DELAY 10
#endif // !INITIAL_DELAY

#ifndef COMMAND_STREAM
#define COMMAND_STREAM Serial
#endif // !COMMAND_STREAM

#ifndef DEVICE_NAME
#define DEVICE_NAME "RingO"
#endif // !DEVICE_NAME

#ifndef VERSION_MAJOR
#define VERSION_MAJOR 1
#endif // !VERSION_MAJOR

#ifndef VERSION_MINOR
#define VERSION_MINOR 0
#endif // !VERSION_MINOR

#ifndef VERSION_PATCH
#define VERSION_PATCH 0
#endif // !VERSION_PATCH

#ifndef DEFAULT_FADE_TRANSITION_STATUS
#define DEFAULT_FADE_TRANSITION_STATUS true
#endif // !DEFAULT_FADE_TRANSITION_STATUS

#if DELAY_BETWEEN_TRANSITIONS <= 0
#error "DELAY_BETWEEN_TRANSITIONS can't be null or negative"
#endif // DELAY_BETWEEN_TRANSITIONS <= 0

#if FRAME_NUMBER == 0
#warning "Defining a buffer of 0 frames means the controller won't be able to dynamically update the led colors"
#endif // FRAME_NUMBER == 0

#if FRAME_NUMBER < 0
#error "Number of buffered frames can't be negative"
#endif // FRAME_NUMBER < 0

#if defined(F_CPU) && F_CPU < 1000000L
#error "CPU Frequency below 1MHz is not supported"
#endif // defined(F_CPU) && F_CPU < 1000000L

uint32_t max_current = CIRCUIT_MAX_CURRENT;
CRGB leds[NUM_LEDS];
size_t interval_between_refresh = DELAY_BETWEEN_TRANSITIONS;
Stream & command_stream = COMMAND_STREAM;
bool enable_fade_transition = DEFAULT_FADE_TRANSITION_STATUS;
volatile bool lock_data = false;
#if ENABLE_STREAM_CONTROLLER
CRGB frames[FRAME_NUMBER][NUM_LEDS];
size_t frame_index = 0;
size_t frame_number = 0;
bool lock_frames = false;
#endif // ENABLE_STREAM_CONTROLLER
#if ENABLE_STORAGE
EEPROMClass & storage = EEPROM;
#endif // ENABLE_STORAGE

#if ENABLE_STORAGE
#define RESTORE_FROM_EEPROM     0x1

/**
 * \brief Load configuration from internal storage
 * \param eeprom A reference on an EEPROM-like storage
 * \return true if the load succeeded else return false
 */
bool loadFromStorageMaxCurrent(EEPROMClass & eeprom, size_t & offset) {
    eeprom.get(offset, max_current);
    offset += sizeof(max_current);
    return true;
}

/**
 * \brief Load configuration from internal storage
 * \param eeprom A reference on an EEPROM-like storage
 * \return true if the load succeeded else return false
 */
bool loadFromStorageIntervalBetweenRefresh(EEPROMClass & eeprom, size_t & offset) {
    eeprom.get(offset, interval_between_refresh);
    offset += sizeof(interval_between_refresh);
    return true;
}

/**
 * \brief Load frames from internal storage
 * \param eeprom A reference on an EEPROM-like storage
 * \return true if the load succeeded else return false
 */
bool loadFromStorageFrames(EEPROMClass & eeprom, size_t & offset) {
    eeprom.get(offset, frame_number);
    offset += sizeof(frame_number);
    if (eeprom.length() <= (offset + sizeof(frames))) {
        return false;
    }
    for (size_t index = 0; index < frame_number; index++) {
        eeprom.get(offset, frames[index]);
        offset += sizeof(index);
    }
    return true;
}

/**
 * \brief Load configuration from internal storage
 * \param eeprom A reference on an EEPROM-like storage
 * \return true if the load succeeded else return false
 */
bool loadFromStorageTransition(EEPROMClass & eeprom, size_t & offset) {
    eeprom.get(offset, enable_fade_transition);
    offset += sizeof(enable_fade_transition);
    return true;
}

/**
 * \brief Load frames and configuration from internal storage
 * \param eeprom A reference on an EEPROM-like storage
 * \return true if the load succeeded else return false
 */
bool loadFromStorage(EEPROMClass & eeprom) {
    size_t  offset = 0;
    uint8_t flags = eeprom.read(offset);

    lock_frames = true;
    if (!(flags & RESTORE_FROM_EEPROM)) {
        return false;
    }
    offset += sizeof(flags);
    if (!loadFromStorageTransition(eeprom, offset) ||
        !loadFromStorageMaxCurrent(eeprom, offset) ||
        !loadFromStorageIntervalBetweenRefresh(eeprom, offset) ||
        !loadFromStorageFrames(eeprom, offset)) {
        lock_frames = false;
        return false;
    }
    frame_index = 0;
    memcpy(leds, frames[0], sizeof(leds));
    lock_frames = false;
    return true;
}

/**
 * \brief Save configuration from internal storage
 * \param eeprom A reference on an EEPROM-like storage
 * \return true if the save succeeded else return false
 */
bool saveInStorageMaxCurrent(EEPROMClass & eeprom, size_t & offset) {
    eeprom.put(offset, max_current);
    offset += sizeof(max_current);
    return true;
}

/**
 * \brief Save configuration from internal storage
 * \param eeprom A reference on an EEPROM-like storage
 * \return true if the save succeeded else return false
 */
bool saveInStorageIntervalBetweenRefresh(EEPROMClass & eeprom, size_t & offset) {
    eeprom.put(offset, interval_between_refresh);
    offset += sizeof(interval_between_refresh);
    return true;
}

/**
 * \brief Save frames from internal storage
 * \param eeprom A reference on an EEPROM-like storage
 * \return true if the save succeeded else return false
 */
bool saveInStorageFrames(EEPROMClass & eeprom, size_t & offset) {
    eeprom.put(offset, frame_number);
    offset += sizeof(frame_number);
    if (eeprom.length() <= (offset + sizeof(frames))) {
        return false;
    }
    for (size_t index = 0; index < frame_number; index++) {
        eeprom.put(offset, frames[index]);
        offset += sizeof(index);
    }
    return true;
}

/**
 * \brief Save configuration from internal storage
 * \param eeprom A reference on an EEPROM-like storage
 * \return true if the save succeeded else return false
 */
bool saveInStorageTransition(EEPROMClass & eeprom, size_t & offset) {
    eeprom.put(offset, enable_fade_transition);
    offset += sizeof(enable_fade_transition);
    return true;
}

/**
 * \brief Save frames and configuration from internal storage
 * \param eeprom A reference on an EEPROM-like storage
 * \return true if the save succeeded else return false
 */
bool saveInStorage(EEPROMClass & eeprom) {
    size_t  offset = 0;
    uint8_t flags = RESTORE_FROM_EEPROM;

    lock_frames = true;
    eeprom.update(offset, flags);
    offset += sizeof(flags);
    if (!saveInStorageTransition(eeprom, offset) ||
        !saveInStorageMaxCurrent(eeprom, offset) ||
        !saveInStorageIntervalBetweenRefresh(eeprom, offset) ||
        !saveInStorageFrames(eeprom, offset)) {
        lock_frames = false;
        return false;
    }
    frame_index = 0;
    lock_frames = false;
    return true;
}

/**
 * \brief Erase content of the storage passed in parameter
 * \return true once the storage is erased
 */
bool eraseStorage(EEPROMClass & eeprom) {
    for (int i = 0; i < (eeprom.length() - 1); i++) {
        EEPROM.write(i, 0);
    }
    return true;
}
#endif // ENABLE_STORAGE

/**
 * \brief Fill the current frame with default application colors
 */
void setDefaultStripColors() {
    static const CRGB colors[] = {
        CRGB::Crimson,
        CRGB::Orange,
        CRGB::PaleGoldenrod,
        CRGB::LimeGreen,
        CRGB::SkyBlue,
        CRGB::BlueViolet,
        CRGB::DarkViolet
    };

    for (size_t index = 0; index < NUM_LEDS; index++) {
        leds[index] = colors[index % (sizeof(colors) / sizeof(CRGB))];
    }
}

/**
 * \brief Reset the controller to startup default configuration
 */
void resetStrip() {
#if ENABLE_STORAGE
    if (!loadFromStorage(EEPROM)) {
#endif // ENABLE_STORAGE
        setDefaultStripColors();
#if ENABLE_STREAM_CONTROLLER
        frame_number = 0;
        memset(frames, 0, sizeof(frames));
#endif // ENABLE_STREAM_CONTROLLER
#if ENABLE_STORAGE
    }
#endif // ENABLE_STORAGE
#if ENABLE_STREAM_CONTROLLER
    frame_index = 0;
#endif // ENABLE_STREAM_CONTROLLER
}

#if ENABLE_STREAM_CONTROLLER
#define COMMAND_SEPARATOR       ' '
#define COMMAND_DELIMITER       '\n'
#define COMMAND_ALT_DELIMITER   '\r'
#define ACK_COMMAND             'A'
#define NACK_COMMAND            'N'
#define BRIGHTNESS_COMMAND      'B'
#define FRAME_COMMAND           'F'
#define POWER_COMMAND           'P'
#define CURRENT_LIMIT_COMMAND   'C'
#define IDENTIFY_COMMAND        'I'
#define DELAY_COMMAND           'D'
#define REFRESH_RATE_COMMAND    'R'
#define TRANSITION_COMMAND      'T'
#if ENABLE_STORAGE
#define SAVE_COMMAND            'S'
#define LOAD_COMMAND            'L'
#define ERASE_COMMAND           'E'
#endif // ENABLE_STORAGE

#ifndef STREAM_WAIT_DEFAULT_TIMEOUT
#define STREAM_WAIT_DEFAULT_TIMEOUT 100
#endif // !STREAM_WAIT_DEFAULT_TIMEOUT

/**
 * \brief Wait for a stream to have bytes available for read
 * \warning This function blocks execution until bytes are available to read or timeout
 * \param stream Reference on the stream to wait for bytes available to read
 * \param timeout The duration in milliseconds to wait until the function timeout
 * \return true if the stream has bytes available to read else return false
 */
bool waitForStreamAvailable(Stream & stream,
                            long timeout=STREAM_WAIT_DEFAULT_TIMEOUT) {
    unsigned long expire_in = millis() + timeout;
    while ((!stream.available() || stream.peek() == -1) &&
            millis() < expire_in) {
        FastLED.delay(1);
    }
    return (stream.available() && stream.peek() != -1);
}

/**
 * \brief Sends into a stream a reply to command received previously
 * \param stream A reference on the stream on which to send the response
 * \param status_command A reference on the object representing the response status to send
 * \param command A reference on the command object for which the response is being sent
 * \param data An optional pointer to additional data to send int the response
 */
template <typename T, typename U>
void replyCommandStatus(Stream & stream,
                        const T & status_command,
                        const T & command,
                        const U * data = nullptr
) {
    stream.print(status_command);
    stream.print(COMMAND_SEPARATOR);
    stream.print(command);
    if (data != nullptr) {
        stream.print(COMMAND_SEPARATOR);
        stream.print(*data);
    }
    stream.print(COMMAND_SEPARATOR);
    stream.print(COMMAND_DELIMITER);
}

/**
 * \brief Sends an acknowledgement into a stream for a command
 * \param stream A reference on the strem on which the acknowledegment is being sent
 * \param command A reference on the object representing the command to acknowledge
 * \param data An optional pointer to additional data to send in the acknowledge
 */
template <typename T, typename U>
void acknowledgeCommand(Stream & stream,
                        const T & command,
                        const U * data = nullptr
) {
    replyCommandStatus(stream, ACK_COMMAND, command, data);
}

/**
 * \brief Sends an error response for a command into a stream
 * \param stream A reference on the stream on which the error is being sent
 * \param command A reference on the object representing the command to deny/error
 * \param data An optional pointer to additional data to send in the error
 */
template <typename T, typename U>
void denyCommand(Stream & stream,
                    const T & command,
                    const U * data = nullptr
) {
    replyCommandStatus(stream, NACK_COMMAND, command, data);
}

/**
 * \brief Deduce if the given byte is a end of command delimiter
 * \return true if the byte is a end of command delimiter else return false
 */
constexpr bool isCommandDelimiter(char byte) {
    return (byte == COMMAND_DELIMITER &&
            byte == COMMAND_ALT_DELIMITER);
}

/**
 * \brief Remove all incoming bytes in the given string until a end of command byte is found
 * \param stream A reference on the stream to purge the current command bytes
 */
void purgeCommandIncomingBuffer(Stream & stream) {
    for (int byte = '\0';
        waitForStreamAvailable(stream) &&
            !isCommandDelimiter(byte)
        ;
        byte = stream.read());
}

/**
 * \brief Parse a RGB color component from a stream
 * \param stream A reference from which to parse the color component
 * \param component A reference on the output variable for the parsed value
 * \return true if the function was able to parse successfully a color value else return false
 */
bool parseComponent(Stream & stream, long & component) {
    if (!stream.available() && !waitForStreamAvailable(stream)) {
        return false;
    }
    component = stream.parseInt();
    if (stream.peek() != COMMAND_SEPARATOR ||
        component < 0 ||
        component > LED_COMPONENT_LIMIT
    ) {
        return false;
    }
    stream.read();
    return true;
}

/**
 * \brief Parse a RGB color from a stream
 * \param stream A reference from which a color is parsed
 * \param color A reference on the output color value
 * \return true if the function was able to parse successfully a color value else return false
 */
bool parseColor(Stream & stream, CRGB & color) {
    long red = 0;
    long green = 0;
    long blue = 0;

    if (!stream.available() && !waitForStreamAvailable(stream)) {
        return false;
    }
    if (parseComponent(stream, red) &&
        parseComponent(stream, green) &&
        parseComponent(stream, blue)
    ) {
        color.red = red;
        color.green = green;
        color.blue = blue;
    }
    else {
        purgeCommandIncomingBuffer(stream);
        return false;
    }
    return true;
}

/**
 * \brief Parse a frame from a stream - Is invoked by "F [${red} ${green} ${blue} ]" command
 * where the list of RGB values are sent as strings and must contain at least enough color values to fill a frame
 * \param stream A reference from which the frame is being parsed
 * \return true if the frame was successfully parsed and updated else return false
 */
bool setFrame(Stream & stream) {
    long updated_frame_index = 0;
    CRGB frame[NUM_LEDS];

    if (!stream.available() && !waitForStreamAvailable(stream)) {
        return false;
    }
    updated_frame_index = stream.parseInt();
    if (updated_frame_index < -1 && updated_frame_index >= FRAME_NUMBER) {
        purgeCommandIncomingBuffer(stream);
        return false;
    }
    if (updated_frame_index == -1) {
        resetStrip();
        return false;
    }
    for (size_t index = 0; index < NUM_LEDS; index++) {
        if (!parseColor(stream, frame[index])) {
            purgeCommandIncomingBuffer(stream);
            return false;
        }
    }
    purgeCommandIncomingBuffer(stream);
    if (updated_frame_index >= frame_number) {
        frame_number = updated_frame_index + 1;
    }
    memcpy(frames[updated_frame_index], frame,
            sizeof(frames[updated_frame_index]));
    return true;
}

/**
 * \brief Set the brightness of the LED ring - Is invoked by "B ${value} " command
 * \param stream A reference from which the frame is being parsed
 * \return true if the brightness has been updated else return false
 */
bool setBrightness(Stream & stream) {
    if (!stream.available() && !waitForStreamAvailable(stream)) {
        return false;
    }
    int brightness = stream.parseInt();

    if (brightness > 0 && brightness <= 255) {
        FastLED.setBrightness(brightness);
    }
    else {
        return false;
    }
    return true;
}

/**
 * \brief Toggles the power switch of the LED ring to ON or OFF - Is invoked by "P 0 " command (ON) or "P 1 " command (OFF)
 * \param stream A reference from which the frame is being parsed
 * \return true if the power was successfully switched else return false
 */
bool setPower(Stream & stream) {
    if (!stream.available() && !waitForStreamAvailable(stream)) {
        return false;
    }
    int status = stream.parseInt();

    digitalWrite(POWER_PIN, (status) ? HIGH : LOW);
    return true;
}

/**
 * \brief Set the current limit usable by the LED ring - Is invoked by "C ${value} " command
 * \param stream A reference from which the frame is being parsed
 * \return true if the limit was successfully updated else return false
 */
bool setCurrentLimit(Stream & stream) {
    if (!stream.available() && !waitForStreamAvailable(stream)) {
        return false;
    }
    int limit = stream.parseInt();

    if (limit >= 0) {
        max_current = limit;
        FastLED.setMaxPowerInVoltsAndMilliamps(CIRCUIT_VOLTAGE, max_current);
    }
    else {
        return false;
    }
    return true;
}

/**
 * \brief Sends on the stream information about the controller - Is invoked by "I " command
 *
 * Sends a string with the following format "I ${device_name} ${version_major} ${version_minor} ${version_patch} "
 * \param stream A reference from which the frame is being parsed
 * \return true
 */
bool identifyDevice(Stream & stream) {
    stream.print(IDENTIFY_COMMAND);
    stream.print(COMMAND_SEPARATOR);
    stream.print(DEVICE_NAME);
    stream.print(COMMAND_SEPARATOR);
    stream.print(VERSION_MAJOR);
    stream.print(COMMAND_SEPARATOR);
    stream.print(VERSION_MINOR);
    stream.print(COMMAND_SEPARATOR);
    stream.print(VERSION_PATCH);
    stream.print(COMMAND_SEPARATOR);
    stream.print(COMMAND_DELIMITER);
    return true;
}


/**
 * \brief Parse and set the delay between two frames displayed on the LED ring - Is invoked by "D ${delay} " command
 * \param stream A reference from which the frame is being parsed
 * \return true if the delay was successfully updated else return false
 */
bool setInterval(Stream & stream) {
    if (!stream.available() && !waitForStreamAvailable(stream)) {
        return false;
    }
    int interval = stream.parseInt();

    if (interval > 0) {
        interval_between_refresh = interval;
        return true;
    }
    return false;
}

/**
 * \brief Parse and set the refresh rate at which the controller change the displayed frame - Is invoked by "R ${rate} " command
 * \param stream A reference on the stream from which the frame is being parsed
 * \return true if the rate was successfully updated else return false
 */
bool setRefreshRate(Stream & stream) {
    if (!stream.available() && !waitForStreamAvailable(stream)) {
        return false;
    }
    int rate = stream.parseInt();

    if (rate > 0) {
        interval_between_refresh = 1000 / rate;
        return true;
    }
    return false;
}

/**
 * \brief Parse and set if the transition is enabled or not - Is invoked by 'T ${0,1} " command
 * \param stream A reference on the stream from which the setting is being parsed
 * \return true
 */
bool setEnableTransition(Stream & stream) {
    if (!stream.available() && !waitForStreamAvailable(stream)) {
        return false;
    }
    enable_fade_transition = stream.parseInt();
    return true;
}

#if ENABLE_STORAGE
/**
 * \brief Save current configuration in device memory - Is invoked by 'S' command
 * \return true if the operation succeeded else return false
 */
bool backupConfiguration(Stream & stream) {
    (void)stream;
    return saveInStorage(storage);
}

/**
 * \brief Load configuration from device memory - Is invoked by 'L' command
 * \return true if the operation succeeded else return false
 */
bool restoreConfiguration(Stream & stream) {
    (void)stream;
    return loadFromStorage(storage);
}

/**
 * \brief Erase configuration from device memory - Is invoked by 'E' command
 * \return true if the operation succeeded else return false
 */
bool eraseConfiguration(Stream & stream) {
    (void)stream;
    return eraseStorage(storage);
}
#endif // ENABLE_STORAGE

using CommandPointer = bool (*)(Stream &);

/**
 * \brief Deduce which command is invoked and fill the function pointer with the corresponding handle function
 * \param input A byte containing the command that is attempted to execute
 * \param command A reference on the output function pointer which is set by the corresponding function if the command is recognized
 */
void deduceCommand(char input, CommandPointer & command) {
    if (input == FRAME_COMMAND) { command = &setFrame; }
    else if (input == BRIGHTNESS_COMMAND) { command = &setBrightness; }
    else if (input == POWER_COMMAND) { command = &setPower; }
    else if (input == CURRENT_LIMIT_COMMAND) { command = &setCurrentLimit; }
    else if (input == IDENTIFY_COMMAND) { command = &identifyDevice; }
    else if (input == DELAY_COMMAND) { command = &setInterval; }
    else if (input == REFRESH_RATE_COMMAND) { command = &setRefreshRate; }
    else if (input == TRANSITION_COMMAND) { command = &setEnableTransition; }
#if ENABLE_STORAGE
    else if (input == SAVE_COMMAND) { command = &backupConfiguration; }
    else if (input == LOAD_COMMAND) { command = &restoreConfiguration; }
    else if (input == ERASE_COMMAND) { command = &eraseConfiguration; }
#endif // ENABLE_STORAGE
}

/**
 * \brief Execute commands invoked in the given stream
 * \param stream A reference from which the frame is being parsed
 */
void executeCommands(Stream & stream) {
    CommandPointer command;
    char input;

    while (stream.available()) {
        if (stream.peek() == COMMAND_DELIMITER ||
            stream.peek() == COMMAND_ALT_DELIMITER
        ) {
            stream.read();
        }
        else {
            command = nullptr;
            input = stream.read();
            stream.read(); // remove command delimiter
            deduceCommand(input, command);
            lock_data = true;
            if (command != nullptr && command(stream)) {
                acknowledgeCommand<char, char>(stream, input);
            }
            else {
                denyCommand<char, char>(stream, input);
            }
            lock_data = false;
        }
    }
}

/**
 * \brief Switch the frame displayed on the LED ring to the next one in the buffer
 */
void nextFrame() {
    if (frame_number == 0) {
        return;
    }
    frame_index++;
    if (frame_index >= frame_number) {
        frame_index = 0;
    }
    memcpy(leds, frames[frame_index], sizeof(leds));
}
#endif // ENABLE_STREAM_CONTROLLER

/**
 * \brief Play the fading transition on the LED ring
 * \param remaining_time The remaining time in milliseconds until the next frame is displayed
 */
void fadeTransition(size_t remaining_time) {
    float brightness;

    if (remaining_time > interval_between_refresh) {
        return;
    }
    else if (remaining_time < (interval_between_refresh >> 1)) {
        brightness = (((float)remaining_time) /
                        (interval_between_refresh >> 1)) * 255;
    }
    else {
        brightness = (((float)(interval_between_refresh - remaining_time)) /
                        (interval_between_refresh >> 1)) * 255;
    }
    if (brightness > 255.) {
      brightness = 255;
    }
    FastLED.setBrightness((int)brightness);
}

#ifdef __SAM3X8E__
void dueLedTimerHandler();
#endif // __SAM3X8E__

/**
 * \brief Configure hardware timer to trigger led ring refresh as a fraction of second
 */
template <size_t F>
void setupLedTimer() {
#ifdef __AVR__
    uint16_t fraction = F;

    if (fraction >= (977 * (F_CPU / 1000000L))) {
        fraction = 1;
    }
    noInterrupts();
    TCCR1A = 0;
    TCCR1B = 0;
    TCNT1 = 0;
    OCR1A = ((977 * (F_CPU / 1000000L)) / F) - 1;
    TCCR1B |= (1 << WGM12);
    TCCR1B |= (1 << CS12) | (1 << CS10);
#ifdef __AVR_ATmega8__
    TIMSK |= (1 << OCIE1A);
#else
    TIMSK1 |= (1 << OCIE1A);
#endif // __ATMEGA8__
    interrupts();
#elif defined(__SAM3X8E__)
    Timer4
        .attachInterrupt(dueLedTimerHandler)
        .setFrequency(F)
        .start()
     ;
#endif // __AVR__
}

void setup() {
    delay(INITIAL_DELAY);
#if ENABLE_STREAM_CONTROLLER
    Serial.begin(SERIAL_BAUDRATE);
#endif // ENABLE_STREAM_CONTROLLER
    FastLED.setMaxPowerInVoltsAndMilliamps(CIRCUIT_VOLTAGE, max_current);
    FastLED.setBrightness(LED_BRIGHTNESS);
    FastLED
        .addLeds<LED_TYPE, DATA_PIN>(leds, NUM_LEDS)
        .setCorrection(TypicalLEDStrip)
    ;
    resetStrip();
    setupLedTimer<LED_TIMER_REFRESH_RATE>();
}

/**
 * \brief Refresh the colors displayed on the LED ring - This function has the biggest priority to run and if the target owns a hardware timer it is used to enforced a minimum call frequency of this function
 */
void loopLedRing() {
    static unsigned long next_loop = static_cast<unsigned long>(-1);

    if (lock_data) {
        return; // Prevent overflowing calls of this function
    }
    lock_data = true;
    if (next_loop == static_cast<unsigned long>(-1)) {
        next_loop = millis() + interval_between_refresh;
    }
#if ENABLE_STREAM_CONTROLLER
    if (millis() > next_loop) {
        nextFrame();
        next_loop = millis() + interval_between_refresh;
    }
#endif // ENABLE_STREAM_CONTROLLER
    if (enable_fade_transition) {
        fadeTransition(next_loop - millis());
    }
    FastLED.show();
    lock_data = false;
}

void loop() {
#if !defined(__AVR__) && !defined(__SAM3X8E__)
    loopLedRing();
#endif // !defined(__AVR__) && !defined(__SAM3X8E__)
#if ENABLE_STREAM_CONTROLLER
    executeCommands(command_stream);
#endif // ENABLE_STREAM_CONTROLLER
}

#ifdef __AVR__
ISR(TIMER1_COMPA_vect) {
    loopLedRing();
}
#endif // __AVR__

#ifdef __SAM3X8E__
void dueLedTimerHandler() {
    loopLedRing();
}
#endif // __SAM3X8E__
